$(document).ready(function () {
    // REGION 1
    const gREQUEST_STATUS_OK = 200; // GET & PUT success
    const gREQUEST_READY_STATUS_FINISH_AND_OK = 4;
    const gREQUEST_STATUS_POST_SUCCESS = 201;
    const gBaseUrl = "http://42.115.221.44:8080/devcamp-pizza365/orders";
    const gURL_DRINK = "http://42.115.221.44:8080/devcamp-pizza365/drinks";
    const gORDER_COLS = ["orderId", "kichCo", "loaiPizza", "idLoaiNuocUong", "thanhTien", "hoTen", "soDienThoai", "trangThai", "action"];
    const gID_COL = 0;
    const gCOMBO_COL = 1;
    const gPIZZA_COL = 2;
    const gDRINK_COL = 3;
    const gMONEY_COL = 4;
    const gNAME_COL = 5;
    const gPHONENUM_COL = 6;
    const gSTATUS_COL = 7;
    const gCHITIET_COL = 8;
    var gIdUpdate = "";
    var gIdDelete = "";
    var gOrderResObj = {
        orders: [],
        filterOrders: function (paramFilter) {
            let vResultFilter = []; // Tạo biến mảng lưu kết quả lọc
            vResultFilter = this.orders.filter(function (item) {
                return ((item.loaiPizza == paramFilter.loaiPizza) && (item.trangThai == paramFilter.trangThai));
            })
            return vResultFilter;
        },
        filterOrdersPizza: function (paramFilter) {
            let vResultFilter = []; // Tạo biến mảng lưu kết quả lọc
            vResultFilter = this.orders.filter(function (item) {
                return ((item.loaiPizza == paramFilter.loaiPizza));
            })
            return vResultFilter;
        },
        filterOrdersStatus: function (paramFilter) {
            let vResultFilter = []; // Tạo biến mảng lưu kết quả lọc
            vResultFilter = this.orders.filter(function (item) {
                return ((item.trangThai == paramFilter.trangThai));
            })
            return vResultFilter;
        }
    }
    var gObjectRequestCreate = {
        kichCo: "",
        duongKinh: "",
        suon: "",
        salad: "",
        loaiPizza: "",
        idVourcher: "",
        idLoaiNuocUong: "",
        soLuongNuoc: "",
        hoTen: "",
        thanhTien: "",
        email: "",
        soDienThoai: "",
        diaChi: "",
        loiNhan: ""
    }
    // REGION 2 

    var gOrderTable = $("#table-order").DataTable({
        columns: [
            { data: gORDER_COLS[gID_COL] },
            { data: gORDER_COLS[gCOMBO_COL] },
            { data: gORDER_COLS[gPIZZA_COL] },
            { data: gORDER_COLS[gDRINK_COL] },
            { data: gORDER_COLS[gMONEY_COL] },
            { data: gORDER_COLS[gNAME_COL] },
            { data: gORDER_COLS[gPHONENUM_COL] },
            { data: gORDER_COLS[gSTATUS_COL] },
            { data: gORDER_COLS[gCHITIET_COL] }
        ],
        columnDefs: [
            {
                targets: gCHITIET_COL,
                className: "text-center",
                defaultContent: `
                <img class="btn-update-detail" src="https://cdn0.iconfinder.com/data/icons/glyphpack/45/edit-alt-512.png" style="width: 20px;cursor:pointer;">
                    <img class="btn-delete-detail" src="https://cdn4.iconfinder.com/data/icons/complete-common-version-6-4/1024/trash-512.png" style="width: 20px;cursor:pointer;">
                        `
            }
        ]
    });
    $("#btn-filter-data").on("click", function () {
        onBtnFilterData();
    });
    $("#btn-create-modal").on("click", function () {
        onBtnCreateClick();
    })
    $(document).on("click", "#table-order .btn-update-detail", function () {
        onBtnUpdateDetailClick(this);
    });
    $(document).on("click", "#table-order .btn-delete-detail", function () {
        onBtnDeleteDetailClick(this);
    });
    $("#btn-update-order").on("click", function () {
        onBtnUpdateOrderClicK();
    });
    $("#btn-confirm-delete-order").on("click", function () {
        onBtnConfirmDeleteClick();
    });
    $("#btn-create-order").on("click", function () {
        onBtnCreateOrderClick();
    });
    // Sự kiện change cho ô select combo
    $("#select-create-combo").on("change", function () {
        loadComboDataToInput($("#select-create-combo").val());
    });

    // REGION 3
    onPageLoading();
    function onPageLoading() {
        $.ajax({
            url: gBaseUrl,
            type: "GET",
            dataType: "json",
            success: function (paramRes) {
                gOrderResObj.orders = paramRes;
                loadDataToTable(paramRes);
                // console.log(paramRes);
            },
            error: function (paramError) {
                console.log(paramError.status);
            }
        });
    }

    function onBtnCreateClick() {
        $("#create-modal").modal("show");
        loadDataToSelectDrinkCreate();
    }

    function onBtnUpdateDetailClick(paramBtn) {
        console.log("%c Nút sửa được ấn", "color: blue");
        "use strict"
        var vRowClick = $(paramBtn).closest("tr");
        var vTable = $("#table-order").DataTable();
        var vOrderId = vTable.row(vRowClick).data().orderId;
        console.log("ID: " + vOrderId);
        $("#update-modal").modal("show");
        loadDataToModalUpdate(vOrderId);
    }

    function onBtnDeleteDetailClick(paramBtn) {
        console.log("%c Nút xóa được ấn", "color: red");
        "use strict"
        var vRowClick = $(paramBtn).closest("tr");
        var vTable = $("#table-order").DataTable();
        var vId = vTable.row(vRowClick).data().id;
        console.log("ID: " + vId);
        gIdDelete = vId;
        $("#delete-modal").modal("show");
    }

    function onBtnFilterData() {
        "use strict"
        var vObjectFilter = {
            loaiPizza: "",
            trangThai: ""
        }
        getDataFromSelect(vObjectFilter);
        if (vObjectFilter.loaiPizza == "all" && vObjectFilter.trangThai == "all") {
            loadDataToTable(gOrderResObj.orders);
        }
        else if (vObjectFilter.trangThai == "all" && vObjectFilter.loaiPizza != "all") {
            let vResultFilter = gOrderResObj.filterOrdersPizza(vObjectFilter);
            loadDataToTable(vResultFilter);
        }
        else if (vObjectFilter.trangThai != "all" && vObjectFilter.loaiPizza == "all") {
            let vResultFilter = gOrderResObj.filterOrdersStatus(vObjectFilter);
            loadDataToTable(vResultFilter);
        }
        else {
            let vResultFilter = gOrderResObj.filterOrders(vObjectFilter);
            loadDataToTable(vResultFilter);
        }
    }

    function onBtnUpdateOrderClicK() {
        var vObjectRequest = {
            trangThai: "" //3 trang thai open, confirmed, cancel
        }

        getDataFromUpdateModal(vObjectRequest);

        var vXmlHttpUpdateOrder = new XMLHttpRequest();
        vXmlHttpUpdateOrder.open("PUT", gBaseUrl + "/" + gIdUpdate);
        vXmlHttpUpdateOrder.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        vXmlHttpUpdateOrder.send(JSON.stringify(vObjectRequest));
        vXmlHttpUpdateOrder.onreadystatechange = function () {
            if (this.readyState == gREQUEST_READY_STATUS_FINISH_AND_OK && this.status == gREQUEST_STATUS_OK) {
                var vUpdatedOrder = vXmlHttpUpdateOrder.responseText;
                console.log(vUpdatedOrder);
                $("#update-modal").modal("hide");
            }
        }
    }

    function onBtnConfirmDeleteClick() {
        $("#delete-modal").modal("hide");
        $.ajax({
            url: gBaseUrl + "/" + gIdDelete,
            type: "DELETE",
            dataType: "json",
            success: function (paramRes) {
                console.log("%c Delete " + gIdDelete + " success!", "color: green");
            },
        });
    }

    function onBtnCreateOrderClick() {
        getDataFromFormCreate(gObjectRequestCreate);
        var vIsValidated = validateDataFromFormCreate(gObjectRequestCreate);

        if (vIsValidated) {
            var vXmlHttpCreateOrder = new XMLHttpRequest();
            vXmlHttpCreateOrder.open("POST", gBaseUrl, true);
            vXmlHttpCreateOrder.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
            vXmlHttpCreateOrder.send(JSON.stringify(gObjectRequestCreate));
            vXmlHttpCreateOrder.onreadystatechange = function () {
                if (this.readyState == gREQUEST_READY_STATUS_FINISH_AND_OK && this.status == gREQUEST_STATUS_POST_SUCCESS) {
                    var vCreatedOrder = vXmlHttpCreateOrder.responseText;
                    console.log(vCreatedOrder);
                    $("#create-modal").modal("hide");
                }
            }
        }
    }
    // REGION 4
    function getListFilter(paramItem) {
        return (paramItem.loaiPizza.toLowerCase() == vFilterParam.pizza.toLowerCase() || vFilterParam.pizza.toLowerCase() == "all") && (paramItem.trangThai.toLowerCase() == vFilterParam.status.toLowerCase() || vFilterParam.status.toLowerCase() == "all")
    }

    function loadDataToTable(paramOrderArr) {
        gOrderTable.clear();
        gOrderTable.rows.add(paramOrderArr);
        gOrderTable.draw();
    }

    function loadDataToModalUpdate(paramOrderId) {
        $.ajax({
            url: gBaseUrl + "/" + paramOrderId,
            type: "GET",
            dataType: "json",
            success: function (paramRes) {
                gIdUpdate = paramRes.id;
                $("#input-update-combo").val(paramRes.kichCo);
                $("#input-update-duongkinh").val(paramRes.duongKinh);
                $("#input-update-suonnuong").val(paramRes.suon);
                $("#input-update-drink").val(paramRes.idLoaiNuocUong);
                $("#input-update-soluong-drink").val(paramRes.soLuongNuoc);
                $("#input-update-voucherId").val(paramRes.idVourcher);
                $("#input-update-pizza").val(paramRes.loaiPizza);
                $("#input-update-salad").val(paramRes.salad);
                $("#input-update-money").val(paramRes.thanhTien);
                $("#input-update-name").val(paramRes.hoTen);
                $("#input-update-email").val(paramRes.email);
                $("#input-update-phonenum").val(paramRes.soDienThoai);
                $("#input-update-address").val(paramRes.diaChi);
                $("#input-update-message").val(paramRes.loiNhan);
                $("#select-update-status").val(paramRes.trangThai);
            },
            error: function (paramError) {
                console.log(paramError.status);
            }
        });
    }

    function getDataFromSelect(paramObj) {
        paramObj.loaiPizza = $("#select-pizza").val();
        paramObj.trangThai = $("#select-status").val();
    }

    function getDataFromUpdateModal(paramObj) {
        paramObj.trangThai = $("#select-update-status").val();
    }

    function loadDataToSelectDrinkCreate() {
        var vDrinkSelect = $("#select-create-drink");
        $.ajax({
            url: gURL_DRINK,
            type: "GET",
            dataType: "json",
            success: function (paramRes) {
                var vDrinkSelect = $("#select-create-drink");
                for (var bIndex = 0; bIndex < paramRes.length; bIndex++) {
                    $("<option>", {
                        text: paramRes[bIndex].tenNuocUong,
                        value: paramRes[bIndex].maNuocUong
                    }).appendTo(vDrinkSelect);
                }
            }
        });
    }

    function getDataFromFormCreate(paramObj) {
        paramObj.kichCo = $("#select-create-combo").val();
        paramObj.duongKinh = $("#input-create-duongkinh").val();
        paramObj.suon = $("#input-create-suonnuong").val();
        paramObj.salad = $("#input-create-salad").val();
        paramObj.loaiPizza = $("#select-create-pizza").val();
        paramObj.idVourcher = $("#input-create-voucherId").val();
        paramObj.idLoaiNuocUong = $("#select-create-drink").val();
        paramObj.soLuongNuoc = $("#input-create-soluong-drink").val();
        paramObj.hoTen = $("#input-create-name").val();
        paramObj.thanhTien = $("#input-create-money").val();
        paramObj.email = $("#input-create-email").val();
        paramObj.soDienThoai = $("#input-create-phonenum").val();
        paramObj.diaChi = $("#input-create-address").val();
        paramObj.loiNhan = $("#input-create-message").val();
    }

    function validateDataFromFormCreate(paramObj) {
        var vIsValidated = true;
        if (!paramObj.hoTen) {
            vIsValidated = false;
            alert("Bạn chưa nhập tên");
        }
        if (!paramObj.soDienThoai) {
            vIsValidated = false;
            alert("Bạn chưa nhập số điện thoại");
        }
        if (!paramObj.diaChi) {
            vIsValidated = false;
            alert("Bạn chưa nhập địa chỉ");
        }
        if (paramObj.kichCo == "none") {
            vIsValidated = false;
            alert("Bạn chưa chọn combo");
        }
        if (paramObj.loaiPizza == "none") {
            vIsValidated = false;
            alert("Bạn chưa chọn loại pizza");
        }
        if (paramObj.idLoaiNuocUong == "none") {
            vIsValidated = false;
            alert("Bạn chưa chọn nước uống");
        }
        return vIsValidated;
    }

    function loadComboDataToInput(paramCombo) {
        var vDuongKinh = 0;
        var vSuon = 0;
        var vSalad = 0;
        var vSoLuongNuoc = 0;
        var vGiaTien = 0;
        switch (paramCombo) {
            case "S":
                vDuongKinh = 20;
                vSuon = 2;
                vSalad = 200;
                vSoLuongNuoc = 2;
                vGiaTien = 150000;
                break;
            case "M":
                vDuongKinh = 25;
                vSuon = 4;
                vSalad = 300;
                vSoLuongNuoc = 3;
                vGiaTien = 2000000;
                break;
            case "L":
                vDuongKinh = 30;
                vSuon = 8;
                vSalad = 500;
                vSoLuongNuoc = 4;
                vGiaTien = 250000;
                break;
        }
        $("#input-create-duongkinh").val(vDuongKinh);
        $("#input-create-suonnuong").val(vSuon);
        $("#input-create-salad").val(vSalad);
        $("#input-create-soluong-drink").val(vSoLuongNuoc);
        $("#input-create-money").val(vGiaTien);
    }
});